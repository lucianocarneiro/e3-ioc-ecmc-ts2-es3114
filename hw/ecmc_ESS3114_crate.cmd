############################################################
############# ESS3114 crate for Paolo

# 0  0:0  PREOP  +  EK1100 EtherCAT-Koppler (2A E-Bus)
# 1  0:1  PREOP  +  EL3104 4K. Ana. Eingang +/-10V Diff.
# 2  0:2  PREOP  +  EL1808 8Ch. Dig. Input 24V, 3ms
# 3  0:3  PREOP  +  EL3114 4K. Ana. Eingang 0-20mA Diff.
# 4  0:4  PREOP  +  EL3114 4K. Ana. Eingang 0-20mA Diff.


#ecmcEpicsEnvSetCalc("ASTEMP_NUM", "0")

# SLAVE 0
#Configure EL1100 EtherCAT Coupler
ecmcEpicsEnvSetCalc("ECMC_SLAVE_NUM", "0")
${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=$(ECMC_SLAVE_NUM), HW_DESC=EK1100"

# SLAVE 1
## Configure EL3104
ecmcEpicsEnvSetCalc("ECMC_SLAVE_NUM", "1")
${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=1, HW_DESC=EL3104"

# SLAVE 2
## Configure EL3114
ecmcEpicsEnvSetCalc("ECMC_SLAVE_NUM", "2")
${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=2, HW_DESC=EL1808"

# SLAVE 3
## Configure EEL3114
ecmcEpicsEnvSetCalc("ECMC_SLAVE_NUM", "3")
${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=3,ECMC_CONFIG_ROOT=$(E3_CMD_TOP)/hw/,  HW_DESC=EL3114"

#

#dbLoadTemplate("$(E3_CMD_TOP)/db/Temperature_slave_$(ECMC_SLAVE_NUM).substitutions","P=Stub-100:, ECMC_P=$(IOC), SLAVE_ID=$(ECMC_SLAVE_NUM)")
