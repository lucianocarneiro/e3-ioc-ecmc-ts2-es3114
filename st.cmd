##############################################################################
## EtherCAT Motion Control Acc Stubs 100 Temperature Measurement

##############################################################################
## Initiation:
epicsEnvSet("IOC" ,"$(IOC="TEST:Ctrl-ECAT-001")")
epicsEnvSet("ECMCCFG_INIT" ,"")  #Only run startup once (auto at PSI, need call at ESS), variable set to "#" in startup.cmd
epicsEnvSet("SCRIPTEXEC" ,"$(SCRIPTEXEC="iocshLoad")")
epicsEnvSet("ECMC_VER" ,"8.0.0")
#epicsEnvSet("NAMING", "ESSnaming")

require ecmccfg

# run module startup.cmd (only needed at ESS  PSI auto call at require)
$(ECMCCFG_INIT)$(SCRIPTEXEC) ${ecmccfg_DIR}startup.cmd, "IOC=$(IOC),ECMC_VER=$(ECMC_VER="8.0.0"),stream_VER=2.8.22,EC_RATE=200,ECMC_ASYN_PORT_MAX_PARAMS=10000"

##############################################################################
# Configure hardware:

ecmcFileExist($(E3_CMD_TOP)/hw/ecmc_ESS3114_crate.cmd,1)
# NCL 1
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmc_ESS3114_crate.cmd

# Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

##############################################################################
############# Configure diagnostics:

ecmcConfigOrDie "Cfg.EcSetDiagnostics(1)"
ecmcConfigOrDie "Cfg.EcEnablePrintouts(0)"
ecmcConfigOrDie "Cfg.EcSetDomainFailedCyclesLimit(100)"

# go active
$(SCRIPTEXEC) ($(ecmccfg_DIR)setAppMode.cmd)

iocInit()

